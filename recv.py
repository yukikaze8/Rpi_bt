# Server Receving

import bluetooth
from btctl import*
import bluetooth_ft
from cprint import*

from picamera import PiCamera
import time

bd = btctl()
bd.on()
bd.aon()
bd.don()

server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

port = 1
server_sock.bind(("",port))
server_sock.listen(1)
cprint('server sock ready','g')

client_sock,address = server_sock.accept()
print "Accepted connection from ",address

bluetooth_ft.recv(client_sock)

client_sock.close()
server_sock.close()

bd.doff()
bd.aoff()
bd.off()
