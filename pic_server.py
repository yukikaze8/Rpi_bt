# Server side of sending pics

import bluetooth
from btctl import*

bd = btctl()
bd.on()
bd.aon()
bd.don()

server_sock=bluetooth.BluetoothSocket(bluetooth.RFCOMM)

port = 1
server_sock.bind(("",port))
server_sock.listen(1)
print 'server sock ready'

client_sock,address = server_sock.accept()
print "Accepted connection from ",address

pic_name = client_sock.recv(1024)
print "Receiving: ",pic_name
client_sock.send(pic_name)

par = 0
data = '0'
ed = ''
# Starts receiving and piping data
with open(pic_name,'w') as f:
    print 'opened ', pic_name
    while True:
        if not data:
            print 'breaking'
            break

        par += 1
        if par >= 10:
            par = 1

        data = client_sock.recv(1024)
        #print 'received size', len(data)
        #print 'data type: ',type(data[-1])
        print 'data par: ',repr(data[-1])
        if int(data[-1]) == 0:
                f.write(data[:-1])
                client_sock.send('-1')
                print 'breaking'
                break
        while int(data[-1]) != par:
            data = client_sock.recv(1024)
            print data[-1]
        print 'received data ',par
        f.write(data[:-1])
        # While this thing so the par is definitely sent
        print 'par sent: ',client_sock.send(str(par))

print 'closing'
f.close()
client_sock.close()
server_sock.close()

bd.doff()
bd.aoff()
bd.off()

