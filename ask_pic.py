# Client asks for picture and receive them

import bluetooth
from btctl import*
import bluetooth_ft
from cprint import*
import time

bd = btctl()
bd.on()
bd.aon()
bd.don()

bd_addr = "B8:27:EB:F7:2A:E8"

port = 1

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((bd_addr, port))

print "Connected to ", bd_addr
        
choice = ''
while choice != 'n':
    
    choice = raw_input("type 'y' to get pic, 'n' to quit\n>")
    if choice == 'y':
        sock.send('y')
        time.sleep(5)
        bluetooth_ft.recv(sock)

    if choice == 'n':
        sock.send('n')


sock.close()

bd.doff()
bd.aoff()
bd.off()
bd.close()
