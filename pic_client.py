# The client side sending the pics

import bluetooth
import time
import sys
from btctl import*

bd = btctl()
bd.on()
bd.aon()
bd.don()

try:
    pic_name = sys.argv[1]
except IndexError:
    print >> sys.stderr, "Error: Needs file name"
    sys.exit(1)

bd_addr = "B8:27:EB:F7:2A:E8"

port = 1

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((bd_addr, port))

print "Connected to", bd_addr
print "Attempting to send", pic_name

sock.send(pic_name)
while(sock.recv(1024) != pic_name):
    pass

par = 0
# Starts parsing and writing data
with open(pic_name,'r') as f:
    print "opened ",pic_name, " for transmission.."
    while True:
        par += 1 
        if par >= 10:
            par = 1
        data = f.read(1000)
        #print 'end of file is: ',repr(data[-1])
        if len(data) < 1000:
            par = 0
        data += str(par)
        print 'end par: ',repr(data[-1])
        #print 'send size in str_len: ',len(data)
        #print 'sent data ',par
        print 'sent size: ',sock.send(data)

        while(int(sock.recv(1024)) != par):
            sock.send(data)
            print 'resending',data[-1]
        if data[-2] == "":
            print 'breaking'
            break

f.close()
sock.close()

bd.doff()
bd.aoff()
bd.off()
bd.close()
