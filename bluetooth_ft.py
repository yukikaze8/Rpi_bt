# Server side of bluetooth file transfer

import bluetooth
import sys
import os
import signal
import time
from cprint import*

class MyException(Exception):
    pass

class TimeoutException(Exception):
    pass

def timeout_handler(signum, frame):
    raise TimeoutException('Time out')

signal.signal(signal.SIGALRM, timeout_handler)

def send(sock, file_name, data_rate=1000):
    try:
        with open(file_name,'rb') as my_file:
            
            file_size = os.path.getsize(file_name)
            print 'Sending:',file_name,file_size,'bytes','at'\
                    ,data_rate, 'bytes per packet...'

            sock.send(file_name)
            sock.send(str(file_size))
            sock.send(str(data_rate))

            if data_rate >= 1008:
                cprint("Warning! High data rate may cause error!")

            parts = (int(file_size)/int(data_rate)) + 1
            print 'Data will be sent in',parts,'parts'

            flag = True
            data_rate = int(data_rate)-1
            my_par = 0
            their_par = -1
            current_part = 0

            while flag:
                my_par = (my_par % 9) + 1
                current_part += 1

                if current_part == parts:
                    my_par = 0
                    flag = False

                data = my_file.read(data_rate)
                data += str(my_par)

                #print 'Sending part',current_part,'of',parts
                #sock.send(data)
                #print 'Receiving par'
                #their_par = sock.recv(1024)
                #print 'their par', their_par

                while(int(their_par) != my_par):
                    
                    signal.alarm(1)
                    try:
                        print 'Sending part',current_part,'of',parts
                        sock.send(data)
                        their_par = sock.recv(1024)
                        pass
                    except TimeoutException as e:
                        cprint(e,'y')
                    else:
                        signal.alarm(0)
                    

    except IOError as e:
        cprint('Error!','r')
        cprint(e,'y')
        return

    my_file.close()
    cprint('File sent','g')

def recv(sock):
    
    file_name = sock.recv(1024)
    file_size = sock.recv(1024)
    data_rate = sock.recv(1024)

    try:
        with open(file_name,'w') as my_file:
            print 'Receiving:',file_name,file_size,'bytes','at'\
                ,data_rate, 'bytes per packet...'

            flag = True
            data_rate = int(data_rate)
            my_par = 0
            their_par = -1
            current_part = 0
            parts = (int(file_size)/int(data_rate)) + 1

            while flag:
                my_par = (my_par % 9) + 1
                current_part += 1

                if current_part == parts:
                    my_par = 0
                    flag = False

                #print 'Receiving part',current_part,'of',parts
                #data = sock.recv(data_rate)
                #print 'data par',data[-1]
                #print 'my par', my_par
                #their_par = data[-1]
                #print 'their par',their_par
                #sock.send(their_par)

                while(int(their_par) != my_par):
                    signal.alarm(1)
                    try:
                        print 'Receiving part',current_part,'of',parts
                        data = sock.recv(data_rate)
                        their_par = data[-1]
                        sock.send(their_par)

                    except TimeoutException as e:
                        cprint(e,'y')
                    else:
                        signal.alarm(0)

                my_file.write(data[:-1])

    except IOError as e:
        cprint('Error!','r')
        cprint(e,'y')
        return

    my_file.close()
    cprint('File received','g')
