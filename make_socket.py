# Creating sockets
# Based off code by A.Huang

from bluetooth import*
from cprint import*
import sys

def rfcomm_server(service_name):

    server_sock=BluetoothSocket(RFCOMM) 
    server_sock.bind(("",1))
    server_sock.listen(1)

    uuid = "1e0ca4ea-299d-4335-93eb-27fcfe7fa848" 

    advertise_service(server_sock,'banana', uuid) #add uuid

    client_sock, client_info = server_sock.accept()
    print "Accepted connection from ", client_info

    return client_sock, server_sock

def rfcomm_client(service_name):
    
    service_matches = find_service(name = service_name)

    if len(service_matches) == 0:
       cprint('Could not find service','r')
       sys.exit(0)

    first_match = service_matches[0]
    port = first_match["port"]
    name = first_match["name"]
    host = first_match["host"]

    print "connecting to", host
    print "name is", name
    print "port is", port

    sock = BluetoothSocket(RFCOMM)
    sock.connect((host,port))
    
    return sock
