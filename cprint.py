# python print with colors
# k = black, r = red, g = green, y = yellow,
# b = blue, m = magenta, c = cyan, l = light grey

def cprint(string, color):

    if color == 'k':
        print '\x1b[1;30m',string,'\x1b[0;39;49m'

    if color == 'r':
        print '\x1b[1;31m',string,'\x1b[0;39;49m'

    if color == 'g':
        print '\x1b[1;32m',string,'\x1b[0;39;49m'

    if color == 'y':
        print '\x1b[1;33m',string,'\x1b[0;39;49m'

    if color == 'b':
        print '\x1b[1;34m',string,'\x1b[0;39;49m'

    if color == 'm':
        print '\x1b[1;35m',string,'\x1b[0;39;49m'

    if color == 'c':
        print '\x1b[1;36m',string,'\x1b[0;39;49m'

    if color == 'l':
        print '\x1b[1;37m',string,'\x1b[0;39;49m'

