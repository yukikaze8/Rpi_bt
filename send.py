# Client sending

import bluetooth
from btctl import*
import bluetooth_ft
import time

bd = btctl()
bd.on()
bd.aon()
bd.don()

try:
    file_name = sys.argv[1]
except IndexError:
    print >> sys.stderr, "Error: Needs file name"

bd_addr = "B8:27:EB:F7:2A:E8"

port = 1

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((bd_addr, port))

print "Connected to ", bd_addr
print "Attempting to send ", file_name

bluetooth_ft.send(sock, file_name)

sock.close()

bd.doff()
bd.aoff()
bd.off()
bd.close()
