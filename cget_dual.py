# Server side

import bluetooth
import pexpect

bd_init = pexpect.spawn('bluetoothctl')
bd_init.send('power on\n')
bd_init.send('agent on\n')
bd_init.send('discoverable on\n')

server_sock=bluetooth.BluetoothSocket(bluetooth.RFCOMM)

port = 1
server_sock.bind(("",port))
server_sock.listen(1)

client_sock,address = server_sock.accept()
print "Accepted connection from ",address

try:
    while True:
        data = client_sock.recv(1024)
        print data
        client_sock.send(data)

except KeyboardInterrupt:
    print "\nEnding session..."

client_sock.close()
server_sock.close()
