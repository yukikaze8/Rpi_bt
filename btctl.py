# Simple module for handling bluetooth via bluetooth_ctl

import pexpect
import sys

class btctl:

    def __init__(self):
        try:
            self.process = pexpect.spawn('bluetoothctl', timeout=30)
            self.process.expect('Controller \S+ \S+ \S+')
            print self.process.before,self.process.after
        except:
            print >> sys.stderr,("\x1b[1;31mError: \x1b[39;49mCould"
            " not load bluetoothctl")

    def on(self):
        try:
            self.process.send('power on\n')
            self.process.expect('Changing power on succeeded', timeout=30)
            print self.process.after
        except:
            print >> sys.stderr,("\x1b[1;31mError: \x1b[39;49mCould"
            " not power on bluetoothctl")

    def off(self):
        try:
            self.process.send('power off\n')
            self.process.expect('Changing power off succeeded', timeout=30)
            print self.process.after
        except:
            print >> sys.stderr, ("\x1b[1;31mError: \x1b[39;49mCould"
            " not turn off bluetooth")

    def aon(self):
        try:
            self.process.send('agent on\n')
            self.process.expect(['Agent registered',
                'Agent is already registered'], timeout=30)
            print self.process.after
        except:
            print >> sys.stderr, ("\x1b[1;31mError: \x1b[39;49mCould"
            " not turn on agent")

    def aoff(self):
        try:
            self.process.send('agent off\n')
            self.process.expect(['Agent unregistered',
                'No agent is registered'], timeout=30)
            print self.process.after
        except:
            print >> sys.stderr,("\x1b[1;31mError: \x1b[39;49mCold"
            " not turn off agent")

    def don(self):
        try:
            self.process.send('discoverable on\n')
            self.process.expect('Changing discoverable on succeeded',
                    timeout=30)
            print self.process.after
        except:
            print >> sys.stderr,("\x1b[1;31mError: \x1b[39;49m"
                    "Could not turn on discoverable")

    def doff(self):
        try:
            self.process.send('discoverable off\n')
            self.process.expect('Changing discoverable off succeeded',
                    timeout=30)
            print self.process.after
        except:
            print >> sys.stderr,("\x1b[1;31mError: \x1b[39;49m"
                    "Could not turn off discoverable")

    def close(self):
        try:
            self.process.send('quit\n')
            self.process.expect('Controller',timeout=30)
            print self.process.before,self.process.after
            self.process.close()
        except:
            print >> sys.stderr,("\x1b[1;31mError: \x1b[39;49m"
                    "Child process failed to close")
