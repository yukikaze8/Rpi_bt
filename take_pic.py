# Server that takes a picture and sends it to the client

import bluetooth
from btctl import*
import bluetooth_ft
from cprint import*

from picamera import PiCamera
import time

bd = btctl()
bd.on()
bd.aon()
bd.don()

server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

port = 1
server_sock.bind(("",port))
server_sock.listen(1)
cprint('server sock ready','g')

client_sock,address = server_sock.accept()
print "Accepted connection from ",address

camera = PiCamera()
camera.resolution = (600,400)

request = ''
while request != 'n':

    request = client_sock.recv(10)
    if request == 'y':
        now = time.strftime('%m_%d_%Y_%H_%M_%S') 
        file_name = '%s.jpg' %now
        time.sleep(3)
        camera.capture(file_name)
        time.sleep(2)
        bluetooth_ft.send(client_sock, file_name)
     
   

client_sock.close()
server_sock.close()

bd.doff()
bd.aoff()
bd.off()
