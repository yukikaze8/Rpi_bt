# Client side

import bluetooth
import pexpect

bd_init = pexpect.spawn('bluetoothctl')
bd_init.send('power on \n')
bd_init.send('agent on \n')
bd_init.send('discoverable on \n')

bd_addr = "B8:27:EB:F7:2A:E8"

port = 1

sock=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((bd_addr, port))

print "Please enter texts: "

try:
    while True:
        msg = raw_input(">")
        sock.send(msg)
except KeyboardInterrupt:
    print "\nending stream"

sock.close()
